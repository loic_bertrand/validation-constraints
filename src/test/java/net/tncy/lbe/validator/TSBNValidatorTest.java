package net.tncy.lbe.validator;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.Validation;
import javax.validation.Validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TSBNValidatorTest {

    private static Validator validator;

    @BeforeAll
    static void beforeAll() {
        var factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    void validIsbn() {
        // Given
        var book = new BookTest("90-70002-34-5");
        // When
        var violations = validator.validate(book);
        // Then
        assertTrue(violations.isEmpty());
    }

    @Test
    void illegalCharacters() {
        // Given
        var book = new BookTest("90/70002/34/5");
        // When
        var violations = validator.validate(book);
        // Then
        assertFalse(violations.isEmpty());
    }

//    @Test
//    void wrongControlKey() {
//        // Given
//        var book = new BookTest("90-70002-34-5");
//        // When
//        var violations = validator.validate(book);
//        // Then
//        assertFalse(violations.isEmpty());
//    }

    @Test
    void emptyString() {
        // Given
        var book = new BookTest("");
        // When
        var violations = validator.validate(book);
        // Then
        assertFalse(violations.isEmpty());
    }

//    @Test
//    void noBookland() {
//        //    Validation d’un code dont les 3 premiers chiffres ne référence pas “Bookland”.
//        // Given
//        var book = new BookTest("90-70002-34-5");
//        // When
//        var violations = validator.validate(book);
//        // Then
//        assertFalse(violations.isEmpty());
//    }

    private static class BookTest {

        @ISBN
        private final String isbn;

        public BookTest(String isbn) {
            this.isbn = isbn;
        }

        public String getIsbn() {
            return isbn;
        }
    }

}
